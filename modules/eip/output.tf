output "eip_j" {
  value = aws_eip.lb[0].id
}

output "eip_d" {
  value = aws_eip.lb[1].id
}