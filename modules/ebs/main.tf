resource "aws_ebs_volume" "ebs" {
  availability_zone = "us-east-2b"
  size              = 8

  tags = {
    Name = "CICD Project"
  }
}
