terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}

resource "aws_instance" "docker_ec2" {
  ami                    = "ami-0629230e074c580f2"
  instance_type          = "t2.micro"
  availability_zone      = "us-east-2b"
  vpc_security_group_ids = [var.sg_id]
  key_name               = "ocekey"
  tags = {
    Name = "docker"
  }

  /*provisioner "remote-exec" {
        inline = [
            "sudo yum install -y python",
            "sudo yum install docker",
            "sudo usermod -aG docker ec2-user"
        ]
        connection {
            type = "ssh"
            user = "ec2-user"
            host = self.public_ip
            private_key = file("/home/oceane/Bureau/projet_cicd/ocekey.pem")
        }
    }*/
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.docker_ec2.id
  allocation_id = var.eip_id
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = var.ebs_id
  instance_id = aws_instance.docker_ec2.id
}